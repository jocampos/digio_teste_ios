//
//  HttpError.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import Foundation

public enum HttpError: Error {
    case noConnectivity
    case badRequest
    case serverError
    case unauthorized
    case forbidden
    case unexpected
}
