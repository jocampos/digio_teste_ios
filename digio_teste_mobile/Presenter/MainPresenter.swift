//
//  MainPresenter.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import Foundation

struct MainList:Model {
    let products:[product]
    let spotlight:[Spotlight]
    let cash: Cash
}

class MainPresenter {
    
    fileprivate weak var mainView:ViewControllerProtocol?
    
    var dataSource = [MainList](){
        didSet{
            mainView?.reloadData()
        }
    }
    
    func setView(_ mainView:ViewControllerProtocol){
        self.mainView = mainView
    }
    
    func getData(service: AdapterService){
        mainView?.showLoading()
        
        service.get(){  [weak self] result in
            guard self != nil else { return }
            
            switch result {
                case .success(let data):
                    if data == nil {
                        self!.mainView?.showAlert(title: "Atenção", message: "Não existem mais dados para carregar!")
                    }else{
                        self?.appendData((data?.toModel())!)
//                        print(data?.toJson())
                    }
                    self!.mainView?.hideLoading()
                case .failure( _):
                    self!.mainView?.showAlert(title: "Erro", message: "Erro ao carregar a lista de Issues")
                    self!.mainView?.hideLoading()
            }
        }
    }
    
    private func appendData(_ data: MainList){
        dataSource.append(data)
    }
    
    
}
