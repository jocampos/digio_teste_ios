//
//  SpotlightCollectionViewCell.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import UIKit

class SpotlightCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var View: UIView!
    @IBOutlet weak var Image: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        Image.layer.borderWidth = 0.5
        Image.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
        Image.layer.cornerRadius = 15
        Image.clipsToBounds = true
        Image.layer.masksToBounds = true
        
        Image.layer.shadowColor = UIColor.black.cgColor
        Image.layer.shadowOpacity = 0.5
        Image.layer.shadowOffset = .zero
        Image.layer.shadowRadius = 10
        Image.layer.shadowPath = UIBezierPath(rect: View.bounds).cgPath
        Image.layer.shouldRasterize = true
        Image.layer.rasterizationScale = UIScreen.main.scale

    }
}
