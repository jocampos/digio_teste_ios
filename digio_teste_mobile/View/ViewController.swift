//
//  ViewController.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import UIKit
import Kingfisher

protocol ViewControllerProtocol:AnyObject {
    func showLoading()
    func hideLoading()
    func reloadData()
    func showAlert(title:String, message:String)
}

class ViewController: UIViewController, ViewControllerProtocol{
    
    
    @IBOutlet weak var SpotlightsCV: UICollectionView!
    @IBOutlet weak var digioCashImage: UIImageView!
    @IBOutlet weak var ProductsCV: UICollectionView!
    
    var cellSpotlight =  "spotlightcell"
    var cellProducts = "productcell"
    
    fileprivate let service = AdapterService()
    fileprivate let mainPresenter = MainPresenter()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ProductsCV.delegate = self
        ProductsCV.dataSource = self

        SpotlightsCV.delegate = self
        SpotlightsCV.dataSource = self
        
        mainPresenter.setView(self)
        mainPresenter.getData(service: service)
        
        initialize()
    }
    
    func initialize(){
        if mainPresenter.dataSource.count > 0{
            digioCashImage.kf.setImage(with: URL(string: mainPresenter.dataSource[0].cash.bannerURL ), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
            digioCashImage.layer.borderWidth = 0.5
            digioCashImage.layer.borderColor = UIColor(white: 0.5, alpha: 0.5).cgColor
            digioCashImage.layer.cornerRadius = 15
            digioCashImage.clipsToBounds = true
            digioCashImage.layer.masksToBounds = true
        }
        
        configLayoutCollectionView()
    }
    
    func configLayoutCollectionView(){
        
        let spotlightLayout = self.SpotlightsCV.collectionViewLayout as! UICollectionViewFlowLayout
        spotlightLayout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 0, right:  5)
        spotlightLayout.minimumInteritemSpacing = 5
        let Spotlightwidth:CGFloat = (view.frame.size.width * 0.8)
        spotlightLayout.itemSize = CGSize(width: Spotlightwidth, height: 250)
        
        let productLayout = self.ProductsCV.collectionViewLayout as! UICollectionViewFlowLayout
        productLayout.sectionInset = UIEdgeInsets(top: 10, left: 5, bottom: 0, right:  5)
        productLayout.minimumInteritemSpacing = 5
        let productwidth:CGFloat = (self.view.frame.size.width * 0.40)
        productLayout.itemSize = CGSize(width: productwidth, height: productwidth)
        
    }


}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if mainPresenter.dataSource.count > 0{
            if collectionView == self.ProductsCV {
                return mainPresenter.dataSource[0].products.count
            }
            
            return mainPresenter.dataSource[0].spotlight.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.ProductsCV){
            let producCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellProducts, for: indexPath) as! ProductCollectionViewCell
            
            producCell.image.kf.indicatorType = .activity
            producCell.image.kf.setImage(with: URL(string: mainPresenter.dataSource[0].products[indexPath.item].imageURL ), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
            
            return producCell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellSpotlight, for: indexPath) as! SpotlightCollectionViewCell
        
        cell.Image.kf.indicatorType = .activity
        cell.Image.kf.setImage(with: URL(string: mainPresenter.dataSource[0].spotlight[indexPath.item].bannerURL ), placeholder: nil, options: [.transition(.fade(0.7))], progressBlock: nil)
        
        
        
        
        
        return cell
    }
    
    
}

extension ViewController{
    func showLoading() {
//        self.indicator.startAnimating()
    }
    
    func hideLoading() {
//        self.indicator.stopAnimating()
    }
    
    func reloadData(){
        initialize()
        SpotlightsCV.reloadData()
        ProductsCV.reloadData()
    }
    
    func showAlert(title:String, message:String){
        presentAlert(withTitle: title, message: message)
    }
}


