//
//  Product.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import Foundation

struct product: Model {
    let name:String
    let imageURL:String
    let description:String
}
