//
//  Spotlight.swift
//  digio_teste_mobile
//
//  Created by Josaphat Campos Pereira on 02/09/20.
//  Copyright © 2020 Josaphat Campos Pereira. All rights reserved.
//

import Foundation

struct Spotlight: Model {
    let name:String
    let bannerURL:String
    let description:String
}
